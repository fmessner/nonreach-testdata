# nonreach - test data
This repository contains some term rewrite systems and nonreachability problems I have used for testing and comparing different versions of *nonreach*[^1].

* TRSs in `TPDB_trs` are taken from the Termination Problem Data Base (TPDB)[^2] (and converted from .xml format).
* Nonreachability problems in `TPDB_edges` are generated from those TRSs with the help of TTT2.[^3]
* Running `./test` will run *nonreach* on all problems and store output in a `results` directory. It will also output some results (amount of NO/MAYBE results, execution time) on completion.

[^1]: https://bitbucket.org/fmessner/nonreach 
[^2]: http://cl2-informatik.uibk.ac.at/mercurial.cgi/TPDB 
[^3]: http://cl-informatik.uibk.ac.at/ttt2/ 
