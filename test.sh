#!/bin/bash

# possibly replace with input vars
TRS_FOLDER="TPDB_trs"
EDGE_FOLDER="TPDB_edges"
SUBFOLDER="TRS_Standard"
RESULTS="results"

function nr {
  for i in $2
  do
    edge_name=$(basename "$i" .txt).edges.txt
    if [ -e "$EDGE_FOLDER/$SUBFOLDER/$1/$edge_name" ]
    then
      mkdir --parents "$RESULTS/$SUBFOLDER/$1"
      results_name=$(basename "$i" .txt).results.txt
      if [ ! -e "$RESULTS/$SUBFOLDER/$1/$results_name" ]
      then
        # adjust this for different runs
        nonreach -f "$TRS_FOLDER/$SUBFOLDER/$1/$i" -p "$EDGE_FOLDER/$SUBFOLDER/$1/$edge_name" -s "[STG,TCAP]" -l 2 -r 0 --parallel > "$RESULTS/$SUBFOLDER/$1/$results_name"
      fi
    fi
  done
}

function nr_all {
  for f in $1
  do
    nr "$f" "$(ls $TRS_FOLDER/$SUBFOLDER/$f)"
  done
}

time nr_all "$(ls $TRS_FOLDER/$SUBFOLDER)"
cat results/TRS_Standard/*/* | sort | uniq -c