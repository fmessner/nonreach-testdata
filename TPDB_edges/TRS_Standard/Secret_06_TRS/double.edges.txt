plus#(x,s(y)) -> plus#(s(x),y)
plus#(x,s(y)) -> plus#(x,s(s(y)))
plus#(s(x),y) -> 
plus#(s(x),y)
plus#(s(x),y) -> plus#(x,s(s(y)))
ack#(s(x),y) -> ack#(s(x),0())

ack#(s(x),y) -> ack#(s(x),s(y))
ack#(s(x),y) -> ack#(s(x),s(y))
ack#(x,ack(s(x),y)) -> 
ack#(0(),x)
ack#(x,ack(s(x),y)) -> ack#(s(x),0())
ack#(x,ack(s(x),y)) -> 
ack#(s(x),s(y))
ack#(x,ack(s(x),y)) -> ack#(s(x),s(y))
ack#(x,s(0())) -> 
ack#(0(),x)
ack#(x,s(0())) -> ack#(s(x),s(y))
ack#(x,s(0())) -> ack#(s(x),s(y))

plus#(x,s(0())) -> plus#(s(x),y)
ack#(x,x) -> ack#(0(),x)
ack#(x,x) -> 
ack#(s(x),0())
ack#(x,x) -> ack#(s(x),s(y))
ack#(x,x) -> ack#(s(x),s(y))

permute#(ack(x,x),p(x),c()) -> permute#(y,x,c())
permute#(x,y,a()) -> 
permute#(x,y,a())
permute#(x,y,a()) -> permute#(x,y,a())
permute#(isZero(x),x,b()) -> 
permute#(false(),x,b())
permute#(isZero(x),x,b()) -> permute#(false(),x,b())

permute#(isZero(x),x,b()) -> permute#(false(),x,b())
permute#(x,x,a()) -> 
permute#(x,y,a())
permute#(x,x,a()) -> permute#(x,y,a())
