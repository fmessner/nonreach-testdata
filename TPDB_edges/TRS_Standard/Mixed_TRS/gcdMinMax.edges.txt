gcd#(-(s(max(x,y)),s(min(x,y))),s(min(x,y))) -> gcd#(s(x),s(y))
gcd#(
                                                                    -(s(max(x,y)),s(min(x,y))),
                                                                    s
                                                                    (min(x,y))) -> 
gcd#(s(x),s(y))
gcd#(-(s(max(x,y)),s(min(x,y))),s(min(x,y))) -> gcd#(s(x),s(y))

gcd#(-(s(max(x,y)),s(min(x,y))),s(min(x,y))) -> gcd#(s(x),s(y))
-#(s(max(x,y)),s(min(x,y))) -> 
-#(s(x),s(y))
max#(x,y) -> max#(s(x),s(y))
min#(x,y) -> min#(s(x),s(y))

-#(x,y) -> -#(s(x),s(y))
max#(x,y) -> max#(s(x),s(y))
min#(x,y) -> min#(s(x),s(y))
