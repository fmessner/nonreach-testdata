and#(x,z) -> and#(x,or(y,z))
and#(x,z) -> and#(x,or(y,z))
and#(x,y) -> 
and#(x,or(y,z))
and#(x,y) -> and#(x,or(y,z))
and#(not(x),not(y)) -> and#(x,or(y,z))

and#(not(x),not(y)) -> and#(x,or(y,z))
not#(y) -> not#(and(x,y))
not#(y) -> 
not#(and(x,y))
not#(y) -> not#(or(x,y))
not#(y) -> not#(or(x,y))
not#(y) -> 
not#(or(x,y))
not#(x) -> not#(and(x,y))
not#(x) -> not#(and(x,y))
not#(x) -> 
not#(or(x,y))
not#(x) -> not#(or(x,y))
not#(x) -> not#(or(x,y))
not#(y) -> 
not#(and(x,y))
not#(y) -> not#(and(x,y))
not#(y) -> not#(or(x,y))
not#(y) -> 
not#(or(x,y))
not#(y) -> not#(or(x,y))
not#(x) -> not#(and(x,y))
not#(x) -> 
not#(and(x,y))
not#(x) -> not#(or(x,y))
not#(x) -> not#(or(x,y))
not#(x) -> 
not#(or(x,y))
