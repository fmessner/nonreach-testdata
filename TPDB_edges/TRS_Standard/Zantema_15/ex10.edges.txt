R#(a(b()),x) -> R#(x,a(y))
R#(a(b()),x) -> R#(x,b())
L#(x,a(y)) -> L#(a(x),y)

L#(x,a(y)) -> L#(b(),x)
L#(x,a(b())) -> L#(a(x),y)
L#(x,a(b())) -> L#(b(),x)

R#(a(x),y) -> R#(x,a(y))
R#(a(x),y) -> R#(x,b())
